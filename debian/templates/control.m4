# -*-debian-control-*-
# NOTE: debian/control is generated from debian/templates/control.m4
Source: gnustep-base
Maintainer: Debian GNUstep maintainers <pkg-gnustep-maintainers@lists.alioth.debian.org>
Uploaders:
 Eric Heintzmann <heintzmann.eric@free.fr>,
 Alex Myczko <tar@debian.org>,
 Yavor Doganov <yavor@gnu.org>,
Section: gnustep
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 gnustep-make`'V_MAKE,
 gnutls-bin <!nocheck>,
 gobjc`'V_OBJC <!cross>,
 gobjc-for-host`'V_OBJC <cross>,
 graphviz,
 libavahi-client-dev,
 libffi-dev,
 libgnutls28-dev,
 libicu-dev,
 libobjc-`'V_GCC-dev <cross>,
 libxml2-dev,
 libxslt1-dev,
 m4,
 tzdata-legacy,
 zlib1g-dev,
Build-Depends-Indep:
 texinfo <!nodoc>,
 texlive-fonts-recommended <!nodoc>,
 texlive-latex-base <!nodoc>,
 xml-core,
Rules-Requires-Root: no
Standards-Version: 4.7.2
Vcs-Browser: https://salsa.debian.org/gnustep-team/gnustep-base
Vcs-Git: https://salsa.debian.org/gnustep-team/gnustep-base.git
Homepage: http://gnustep.org

Package: gnustep-base-common
Architecture: all
Multi-Arch: foreign
Depends:
 ca-certificates,
 gnustep-common:any`'V_MAKE,
 tzdata-legacy,
 ${misc:Depends},
Breaks:
 gnustep-base-runtime (<< 1.30.0-5),
 gnustep-gui-common (<< 0.31.1-7),
Replaces:
 gnustep-base-runtime (<< 1.30.0-5),
Description: GNUstep Base library - common files
 The GNUstep Base Library is a powerful fast library of
 general-purpose, non-graphical Objective C classes, inspired by the
 OpenStep API but implementing Apple and GNU additions to the API as
 well.
 .
 This package contains the common files needed by the GNUstep Base library.

Package: gnustep-base-runtime
Architecture: any
Multi-Arch: foreign
Depends:
 gnustep-base-common (= ${source:Version}),
 graphviz,
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 libgnustep-base-dev (<< 1.30.0-12),
Replaces:
 libgnustep-base-dev (<< 1.30.0-12),
Description: GNUstep Base library - daemons and tools
 The GNUstep Base Library is a powerful fast library of
 general-purpose, non-graphical Objective C classes, inspired by the
 OpenStep API but implementing Apple and GNU additions to the API as
 well.
 .
 This package contains the runtime support files needed by GNUstep
 applications.

Package: libgnustep-base`'SOV_BASE
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 gnustep-base-common (= ${source:Version}),
 gnustep-multiarch`'V_MAKE,
 gnutls-bin,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 gnustep-base-runtime (= ${binary:Version}),
Breaks:
 unar (<< 1.10.8+ds1-7),
Description: GNUstep Base library
 The GNUstep Base Library is a powerful fast library of
 general-purpose, non-graphical Objective C classes, inspired by the
 OpenStep API but implementing Apple and GNU additions to the API as
 well.  It includes for example classes for unicode strings, arrays,
 dictionaries, sets, byte streams, typed coders, invocations,
 notifications, notification dispatchers, scanners, tasks, files,
 networking, threading, remote object messaging support (distributed
 objects), event loops, loadable bundles, attributed unicode strings,
 xml, mime, user defaults.

Package: libgnustep-base-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 gnustep-base-runtime (= ${binary:Version}),
 gnustep-make`'V_MAKE,
 gobjc++`'V_OBJC | gobjc++-for-host`'V_OBJC,
 libgnustep-base`'SOV_BASE (= ${binary:Version}),
 ${misc:Depends},
Recommends:
 gnustep-base-doc,
Provides:
 gnustep-base-abi-`'SOV_BASE
Breaks:
 libgnustep-corebase-dev (<< 0.1.1+20230710-5),
 libgnustep-dl2-dev (<< 0.12.0+git20171224-4),
 libnetclasses-dev (<< 1.06.dfsg+really1.1.0-3),
 libuniversaldetector-dev (<< 1.1-6),
Description: GNUstep Base header files and development libraries
 This package contains the header files and static libraries required
 to build applications against the GNUstep Base library.
 .
 Install this package if you wish to develop your own programs using
 the GNUstep Base Library.

Package: gnustep-base-doc
Section: doc
Architecture: all
Build-Profiles: <!nodoc>
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Recommends:
 libgnustep-base-dev,
Description: Documentation for the GNUstep Base Library
 This package contains the GNUstep Base Library API reference, as well
 as the GNUstep Base programming manual and GNUstep Coding Standards
 in Info, HTML and PDF format.
